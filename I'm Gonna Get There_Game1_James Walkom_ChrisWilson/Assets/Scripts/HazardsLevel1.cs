﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HazardsLevel1 : MonoBehaviour 
{
		public Rigidbody robotKevin;
		public Vector3 checkpointOne;
		public Vector3 checkpointTwo;
		public Vector3 checkpointThree;
		public Text LifeCount;
		public int LifeNumber;
		
	
		
		// Use this for initialization
		void Start () 
		{
			LifeNumber = 4;
			LifeCount.text = "Life    x  " + LifeNumber.ToString();
			robotKevin = GetComponent<Rigidbody>();
		}
		
		// Update is called once per frame
		void Update () 
		{
			gameOver ();
		}
		void OnCollisionEnter(Collision other)
		{
			if (other.gameObject.tag == "Pit Hazard1")
			{
				
				robotKevin.transform.position = checkpointOne;
				LifeNumber = LifeNumber-1;
				LifeCount.text = "Life   X  " + LifeNumber.ToString();
				
				
			}
			if (other.gameObject.tag == "Pit Hazard2")
			{
				
				robotKevin.transform.position = checkpointTwo;
				LifeNumber = LifeNumber-1;
				LifeCount.text = "Life   X  " + LifeNumber.ToString();
				
				
			}
			if (other.gameObject.tag == "Pit Hazard3")
			{
				
				robotKevin.transform.position = checkpointThree;
				LifeNumber = LifeNumber-1;
				LifeCount.text = "Life   X  " + LifeNumber.ToString();
				
			}
			
		}
		public void gameOver()
		{
			if (LifeNumber <= 0)
			{
			Application.LoadLevel("GameOver");
			}
		}

	
	
}
