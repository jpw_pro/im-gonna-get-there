﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HazardsLevel2 : MonoBehaviour 
{	
	public Rigidbody robotKevin;
	public Vector3 checkpointFour;
	public Vector3 checkpointFive;
	public Text LifeCount;
	public int LifeNumber;
	// Use this for initialization
	void Start () 
	{
		LifeNumber = 4;
		LifeCount.text = "Life   X  " + LifeNumber.ToString();
		robotKevin = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		gameOver();
	}
	
	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "Pit Hazard4")
		{
			robotKevin.transform.position = checkpointFour;
			LifeNumber = LifeNumber-1;
			LifeCount.text = "Life   X  " + LifeNumber.ToString();
		}
		if (other.gameObject.tag == "Pit Hazard5")
		{
			robotKevin.transform.position = checkpointFive;
			LifeNumber = LifeNumber-1;
			LifeCount.text = "Life   X  " + LifeNumber.ToString();
		}
	}
	public void gameOver()
	{
		if (LifeNumber <= 0)
		{
			Application.LoadLevel("GameOver");
		}
	}
	
	
}
