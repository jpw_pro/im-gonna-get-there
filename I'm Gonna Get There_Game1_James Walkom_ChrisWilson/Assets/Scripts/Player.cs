﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public Rigidbody robotKevin;
	public float movementSpeed;
	public float jumpheight;
	public bool canJump;
	public int turnSpeed;
	
	
	
	// Use this for initialization
	void Start () 
	{
	robotKevin = GetComponent<Rigidbody>();
	
	
	
	
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		checkKeyPress ();
		rotateCamera ();
	}
	
public void checkKeyPress()
	{
		if (Input.GetKey (KeyCode.W))
		{
			robotKevin.AddForce (transform.forward * movementSpeed);
		}
		if (Input.GetKey (KeyCode.S))
		{
			robotKevin.AddForce(-transform.forward * movementSpeed);
		}
		if (Input.GetKey(KeyCode.A))
		{
			robotKevin.AddForce(-transform.right *movementSpeed);	
		}
		if (Input.GetKey (KeyCode.D))
		{
			robotKevin.AddForce(transform.right *movementSpeed);
		}
		
		if (Input.GetKeyDown (KeyCode.Space) && canJump)
		{
			robotKevin.AddForce( Vector3.up * jumpheight, ForceMode.Impulse);
		}
		
	}
	public void rotateCamera()
	{
		if (Input.GetKey (KeyCode.RightArrow))
		{
			robotKevin.transform.Rotate(Vector3.up *turnSpeed *Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.LeftArrow))
		{
			robotKevin.transform.Rotate(-Vector3.up *turnSpeed *Time.deltaTime );
		}
	}
	
	
		void OnCollisionStay(Collision other)
		{
			if (other.gameObject.tag == "Ground")
			{
				canJump = true;
			}
			if (other.gameObject.tag == "Moving Platform")
			{
				canJump = true;
			}
			if (other.gameObject.tag == "Disappearing Platform")
			{
				canJump = true;
			}	
		
		}
	
	
	void OnCollisionExit(Collision other)
	{
		if (other.gameObject.tag == "Ground")
		{
			canJump = false;
		}
		if (other.gameObject.tag == "Moving Platform")
		{
			canJump = false;
		}
		if (other.gameObject.tag == "Disappearing Platform")
		{
			canJump = false;
		}	
	}
	
}	
