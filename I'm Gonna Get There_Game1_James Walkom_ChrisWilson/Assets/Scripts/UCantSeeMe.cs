﻿using UnityEngine;
using System.Collections;

public class UCantSeeMe : MonoBehaviour 
{

public Renderer mesh;
void Start ()
{
		
}

void OnTriggerStay (Collider other)
{
	if (other.gameObject.tag == "Wall")
	{ 
			print ("Wall collision");
			other.gameObject.GetComponent<MeshRenderer>().enabled = false;
	}
	
}
void OnTriggerExit (Collider other)
{
	if (other.gameObject.tag == "Wall")
	{
		other.gameObject.GetComponent<MeshRenderer>().enabled = true;	
	}

}

}

