﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {
	
	public Text TenThousand;
	public Text TwentyThousand;
	public Text ThirtyThousand;
	public Text ScoreText;
	public string brackets;
	public int point;
	
	// Use this for initialization
	void Start () 
	{
		brackets = "()";
		point = 0;
		ScoreText.text = "Score: " + point.ToString() + brackets.ToString(); 
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Bolt")
		{
			other.gameObject.SetActive(false);
			brackets = "(10,000)";
			point = point+1000;
			ScoreText.text = "Score: " + point.ToString() + brackets.ToString();
		}
		if (other.gameObject.tag == "Bolt2")
		{
			other.gameObject.SetActive(false);
			brackets = "(20,000)";
			point = point+1000;
			ScoreText.text = "Score: " + point.ToString() + brackets.ToString();
		}
		if (other.gameObject.tag == "Bolt3")
		{
			other.gameObject.SetActive(false);
			brackets = "(30,000)";
			point = point+1000;
			ScoreText.text = "Score: " + point.ToString() + brackets.ToString();
		}
	
		
	}	
	void OnCollisionEnter (Collision other)
	{	
		if (other.gameObject.name == "Door1")
		{
			other.gameObject.SetActive(true);
		}
		if (other.gameObject.name == "Door2")
		{
			if (point >= 10000)
			{
				other.gameObject.SetActive(false);
				
			}
			else if (point <= 9000)
			{
				other.gameObject.SetActive(true);
				TenThousand.gameObject.SetActive(true);
			}
		}
		if (other.gameObject.name == "Door3")
		{
			if (point >= 20000)
			{
				other.gameObject.SetActive(false);
				
				
			}
			else if (point <= 19000)
			{
				other.gameObject.SetActive(true);
				TwentyThousand.gameObject.SetActive(true);
				
			}
		}
		if (other.gameObject.name == "Door4")
			if (point >= 30000)
		{
			other.gameObject.SetActive(false);
			LoadLevelTwo();
			
		}
		else if (point <= 29000)
		{
			other.gameObject.SetActive(true);
			ThirtyThousand.gameObject.SetActive(true);
		}
		if (other.gameObject.name == "Door5")
		{
			if (point >= 20000)
			{
				other.gameObject.SetActive(false);
				IGotThere();
				
				
			}
			else if (point <= 19000)
			{
				other.gameObject.SetActive(true);
				TwentyThousand.gameObject.SetActive(true);
				
			}
		}
	}
	void OnCollisionExit (Collision other)
	{
		if (other.gameObject.name == "Door2")
		{
			if (point <= 9000)
			{
				TenThousand.gameObject.SetActive(false);
			}
		}
		if (other.gameObject.name == "Door3")
		{
			if (point <= 19000)
			{
				TwentyThousand.gameObject.SetActive(false);
			}
		}
		if (other.gameObject.name == "Door4")
			
		if (point <= 29000)
		{
			ThirtyThousand.gameObject.SetActive(false);
		}
		if (other.gameObject.name == "Door5")
			
			if (point <= 19000)
		{
			TwentyThousand.gameObject.SetActive(false);
		}
	}
	
	public void LoadLevelTwo()
		{
			Application.LoadLevel("Level2");
		}
		
		public void IGotThere()
		{
			Application.LoadLevel("IGotThere");
		}
	
}
